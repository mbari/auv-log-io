package org.mbari.auv.io.dsl

import scala.math.toDegrees
import scilube.geometry.Point4D
import scilube.gis.CRSConverterGeoToUTM

sealed abstract class LogType(val key: String)

abstract class PoseType(key: String) extends LogType(key)
case object Navigation extends PoseType("dataNav")

abstract class PositionType(key: String) extends LogType(key)
case object GPS extends PositionType("gps")

abstract class BeamType(key: String) extends LogType(key)
case object DVL extends BeamType("dataDvl")
case object MultiBeam extends BeamType("dataMB")

abstract class VelocityType(key: String) extends LogType(key)
case object Kearfott extends VelocityType("dataK")

abstract class ExtendedVelocityType(key: String) extends LogType(key)
case object NavigationWithKearfott extends ExtendedVelocityType("navWithKearfott")

/**
 * The simplest datum trait. Represents a moment in time using a double. For AUV missions, this double
 * is typically unix seconds (seconds since 1970-01-01 00:00:00 UTC)
 */
trait TimeDatum {
  def time: Double
}

case class TimeStamp(time: Double, index: Int, src: String) extends TimeDatum

/**
 * A position is a point location in time. For most AUV missions, data is represented as eastings (x) and
 * northings (y) in UTM Zone 10N. The exact x, y representation is described by the __crs__ field which
 * describes the Coordinate Reference System.
 */
trait Position extends Point4D[Double, Double] with TimeDatum {

  /**
   * Coordinate Reference System
   * See http://spatialreference.org/ for references. Some common ones:
   * - EPSG:4326 (WGS 84) Horizontal component of 3D system. Used by the GPS satellite navigation system and for NATO military geodetic surveying.
   * - EPSG:4979 (WGS 84) Used by the GPS satellite navigation system. (Includeds vertical axis)
   * - EPSG:32609 (UTM zone 9N) - Large and medium scale topographic mapping and engineering survey. Covers california
   * - EPSG:32610 (UTM zone 10N) - Large and medium scale topographic mapping and engineering survey. Covers california
   */
  def crs: String
  def time: Double
  override def w: Double = time

}

object Position {

  /** WGS 84 */
  val DEFAULT_CRS = "EPSG:4326"

  /** CRS String for WGS 84 */
  val WGS84 = "EPSG:4326"

  /** CRS String for UTM Zone 10N */
  val UTMZONE10N = "EPSG:32610"

  /**
   * Convert a geo position to utmZone 10N. __Note:__ This is a hack and does not actually check the
   * CRS of the source.
   * @param position
   * @return
   */
  def geoToUtmZone10N(position: Position): Position = {
    // TODO check CRS
    val en = CRSConverterGeoToUTM(position.x, position.y)
    SimplePosition(position.time, en._1, en._2, position.z, UTMZONE10N)
  }

}

/**
 * An orientation is an object's orientation (phi, theta, psi) at a moment in time.
 */
trait Orientation extends TimeDatum {
  def phi: Double
  def theta: Double
  def psi: Double
}

/**
 * A Pose is a position and orientation in space-time
 */
trait Pose extends Position with Orientation

object Pose {
  def radiansToDegrees(pose: Pose): Pose = {
    val xd = toDegrees(pose.x)
    val yd = toDegrees(pose.y)
    NavigationPose(pose.time, xd, yd, pose.z, pose.phi, pose.theta, pose.psi)
  }
}

/**
 * A NavigationQC represents navigation quality control flags
 */
trait NavigationQC extends TimeDatum {
  def gpsValid: Boolean
  def dvlValid: Boolean
  def bottomLock: Boolean
}

/**
 * A VelocitySample has x, y, and z velocities and accelerations
 */
trait VelocitySample extends TimeDatum {
  def vx: Double
  def vy: Double
  def vz: Double
  def ax: Double
  def ay: Double
  def az: Double
}

/**
 * Case class that implements Position.
 * @param time
 * @param x
 * @param y
 * @param z
 * @param crs
 */
case class SimplePosition(
  time: Double,
  x: Double,
  y: Double,
  z: Double,
  crs: String = Position.DEFAULT_CRS
) extends Position

case class GeoPositionWithID(
  id: String,
  time: Double,
  x: Double,
  y: Double,
  z: Double,
  crs: String = Position.DEFAULT_CRS
) extends Position

/**
 * Case class for GPS captured positions. This adds 2 fields: quality and numberOfSatellites
 * @param time
 * @param x
 * @param y
 * @param z
 * @param quality
 * @param numberOfSatellites
 * @param crs
 */
case class GPSPosition(
  time: Double,
  x: Double,
  y: Double,
  z: Double,
  quality: Double = 0,
  numberOfSatellites: Int = 1,
  crs: String = Position.DEFAULT_CRS
) extends Position

/**
 * An implementation of Pose that also contains navigatio quality control info. All fields are
 * required by TerrainNav.
 * @param time
 * @param x
 * @param y
 * @param z
 * @param phi
 * @param theta
 * @param psi
 * @param gpsValid
 * @param dvlValid
 * @param bottomLock
 * @param crs
 */
case class NavigationPose(
  time: Double,
  x: Double,
  y: Double,
  z: Double,
  phi: Double,
  theta: Double,
  psi: Double,
  gpsValid: Boolean = false,
  dvlValid: Boolean = false,
  bottomLock: Boolean = false,
  crs: String = Position.DEFAULT_CRS
) extends Pose with NavigationQC

/**
 * A case class for representing a Kearfott sample
 *
 * @param time
 * @param x
 * @param y
 * @param z
 * @param phi
 * @param theta
 * @param psi
 * @param vx mVbodyxK
 * @param vy mVbodyyK
 * @param vz mVbodyzK
 * @param ax mAccelxK
 * @param ay mAccelyK
 * @param az mAccelzK
 * @param crs
 */
case class KearfottPose(
  time: Double,
  x: Double,
  y: Double,
  z: Double,
  phi: Double,
  theta: Double,
  psi: Double,
  vx: Double,
  vy: Double,
  vz: Double,
  ax: Double,
  ay: Double,
  az: Double,
  crs: String = Position.DEFAULT_CRS
) extends Pose with VelocitySample

/**
 * TerrainNav uses a mash-up of Navigation data combined with velocities from the Kearfott. This case
 * class includes all required fields.
 *
 * @param time
 * @param x
 * @param y
 * @param z
 * @param phi
 * @param theta
 * @param psi
 * @param vx
 * @param vy
 * @param vz
 * @param ax
 * @param ay
 * @param az
 * @param gpsValid
 * @param dvlValid
 * @param bottomLock
 * @param crs
 */
case class NavPoseWithVelocity(
  time: Double,
  x: Double,
  y: Double,
  z: Double,
  phi: Double,
  theta: Double,
  psi: Double,
  vx: Double,
  vy: Double,
  vz: Double,
  ax: Double,
  ay: Double,
  az: Double,
  gpsValid: Boolean = false,
  dvlValid: Boolean = false,
  bottomLock: Boolean = false,
  crs: String = Position.DEFAULT_CRS
) extends Pose with NavigationQC with VelocitySample