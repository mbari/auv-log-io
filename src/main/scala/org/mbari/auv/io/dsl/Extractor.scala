package org.mbari.auv.io.dsl

import org.mbari.auv.io.MissionReader.MissionData
import org.mbari.auv.io.{ LogRecord }

/**
 * Base methods for extracting records of interest from [[org.mbari.auv.io.MissionReader.MissionData]]
 * Created by brian on 2/19/14.
 */
trait Extractor {

  def missionToRecords(data: MissionData, instrumentName: String): Seq[LogRecord[Double]] =
    data.values.flatten.toSeq.filter(_.description.instrumentName == instrumentName)

  /**
   * Extracts records from mission
   *
   * @param records
   * @param shortName
   * @return
   */
  def extract(records: Seq[LogRecord[Double]], shortName: String): Option[IndexedSeq[Double]] =
    records.find(_.description.shortName == shortName) match {
      case None => None
      case Some(d) => Option(d.data)
    }

}

trait Extractee {
  def time: Seq[Double]

  def asTimestamps(key: String): Seq[TimeStamp] =
    time.zipWithIndex.map { case (t, i) => TimeStamp(t, i, key) }
}

trait ExtractAs[T] {
  def from(data: MissionData): Seq[T]
}