package org.mbari.auv.io.dsl

/**
 * A container for beam related data (DVL, multibeam)
 * Created by brian on 5/27/14.
 */
trait Beam extends Orientation {
  def ranges: Seq[Double]
}

case class SimpleBeam(
  time: Double,
  phi: Double,
  theta: Double,
  psi: Double,
  ranges: Seq[Double]
) extends Beam