package org.mbari.auv.io.dsl

import org.mbari.auv.io.MissionReader.MissionData

import scala.util.{ Try }
import org.slf4j.LoggerFactory
import scala.util.Success
import scala.util.Failure

/**
 * Extracts Navigation data from [[MissionData]]
 *
 * Created by brian on 2/24/14.
 */
object ExtractNavigation extends Extractor {

  private[this] val log = LoggerFactory.getLogger(getClass)

  def from(data: MissionData): Seq[NavigationDatum] = toPoses(data, Navigation)

  private def toPoses(data: MissionData, instrumentName: String): Seq[NavigationDatum] = {
    val records = missionToRecords(data, instrumentName)

    if (!records.isEmpty) {

      Try {
        val time = extract(records, "time").get
        val x = extract(records, "mPos_x").get
        val y = extract(records, "mPos_y").get

        def munge(v: Option[IndexedSeq[Double]]) = v match {
          case None => time.map(_ => 0D)
          case Some(d) => d
        }

        val z = munge(extract(records, "mDepth"))
        val phi = munge(extract(records, "mPhi"))
        val theta = munge(extract(records, "mTheta"))
        val psi = munge(extract(records, "mPsi"))
        val gpsValid = munge(extract(records, "mGpsValid")).map(_ == 1)
        val dvlValid = munge(extract(records, "mDvlValid")).map(_ == 1)

        for (i <- 0 until time.size) yield {
          NavigationPose(time(i), x(i), y(i), z(i), phi(i), theta(i), psi(i),
            gpsValid(i), dvlValid(i), bottomLock = false, Position.UTMZONE10N)
        }
      } match {
        case Success(d) => d
        case Failure(e) => {
          log.warn("Failed to read navigation data", e)
          Nil
        }
      }

    } else Nil

  }

}
