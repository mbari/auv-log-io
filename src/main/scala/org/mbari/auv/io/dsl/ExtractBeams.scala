package org.mbari.auv.io.dsl

import java.time.Instant
import java.util.Date
import org.mbari.auv.io.MissionReader.MissionData
import scala.util.{ Failure, Success, Try }
import org.slf4j.LoggerFactory

/**
 * Created by brian on 5/27/14.
 */
object ExtractBeams extends Extractor {

  val NOV2010 = Date.from(Instant.parse("2010-11-08T00:00:00Z"))

}

/**
 * Extract DVL data from [[MissionData]]
 */
object ExtractDVL extends Extractor with ExtractAs[DVLDatum] {

  private[this] val log = LoggerFactory.getLogger(getClass)

  def from(data: MissionData): Seq[DVLDatum] = toBeams(data)

  private def toBeams(data: MissionData): Seq[DVLDatum] = {
    val records = missionToRecords(data, DVL)
    if (!records.isEmpty) {
      Try {
        val time = extract(records, "time").get
        val beam1 = extract(records, "dvlBeam1").get
        val beam2 = extract(records, "dvlBeam2").get
        val beam3 = extract(records, "dvlBeam3").get
        val beam4 = extract(records, "dvlBeam4").get
        val phi = extract(records, "dvlRoll").get
        val theta = extract(records, "dvlHeading").get
        val psi = extract(records, "dvlPitch").get
        for (i <- 0 until time.size) yield {
          val ranges = Seq(beam1(i), beam2(i), beam3(i), beam4(i))
          SimpleBeam(time(i), phi(i), theta(i), psi(i), ranges)
        }

      } match {
        case Success(d) => d
        case Failure(e) => {
          log.warn("Failed to read DVL data", e)
          Nil
        }
      }
    } else Nil
  }

}