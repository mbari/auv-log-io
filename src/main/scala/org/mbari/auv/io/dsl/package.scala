package org.mbari.auv.io

import java.io.File

import org.mbari.auv.io.MissionReader._
import scilube.Matlib

import scala.reflect.ClassTag

/**
  * This package provides a simple to use DSL on top of MissionReader that allows for simple, understandable
  * extraction of data from AUV missions and converts the data to data structures that align with TerrainNav's
  * built-in types. An example of usage:
  *
  * {{{
  * // import the DSL. You always just import everything
  * import org.mbari.auv.io.dsl._
  *
  * // Read an AUV mission directory
  * val data = MissionReader.read("/path/to/directory/of/auv/log/files")
  *
  * // Use DSL to convert to different types
  * val nav = data as Navigation
  * val dvl = data as DVL
  * val nk = data as NavKearfott
  * val k = data as Kearfott
  *
  * // Convert data to monotonic (ordered, no repeating values)
  * val mNav = nav monotonic
  *
  * }}}
  *
  *
  * Created by brian on 2/24/14.
  */
package object dsl {

  import scala.collection.immutable.ArraySeq

  type NavigationDatum  = Pose with NavigationQC
  type KearfottDatum    = Pose with VelocitySample
  type NavKearfortDatum = Pose with NavigationQC with VelocitySample
  type DVLDatum         = Beam

  implicit def logTypeToString(t: LogType): String                        = t.key
  implicit def flattenOptionOfSeq[B, A <: Seq[B]](opt: Option[A]): Seq[B] = opt.getOrElse(Nil)

  implicit class TimeDatumSeq[T <: TimeDatum](seq: Seq[T]) {
    def asTimestamps(lt: LogType): Seq[TimeStamp] = {
      seq.zipWithIndex.map { case (t, i) => TimeStamp(t.time, i, lt.key) }
    }
  }

  /**
    * Implicit conversion of a Mission to some data subset. Use as:
    * {{{
    *   val data = MissionReader.read(someDirectory)
    *   val nav = data as Navigation // MissionDataDSL(data).as(Navigation)
    *   val dvl = data as DVL        // MissionDataDSL(data).as(DVL)
    *   val k   = data as Kearfott   // MissionDataDSL(data).as(Kearfott)
    *   val nk  = data as NavigationWithKearfott
    * }}}
    *
    * The general usage is:
    * {{{
    *   missionData as LogType
    *   MissionDataDSL(missionData).as(LogType) // equivalent call
    *
    *   // Example
    *   missionData as DVL
    * }}}
    *
    * @param data The missiondata to munge
    */
  implicit class MissionDataDSL(data: MissionData) {

    def as(t: PoseType): Seq[NavigationDatum] = t match {
      case Navigation => ExtractNavigation.from(data)
    }

    def as(t: PositionType): Seq[Position] = Nil

    def as(t: BeamType): Seq[Beam] = t match {
      case DVL       => ExtractDVL.from(data)
      case MultiBeam => Nil // TODO
    }

    def as(t: VelocityType): Seq[KearfottDatum] = t match {
      case Kearfott => ExtractKearfott.from(data)
    }

    def as(t: ExtendedVelocityType) = t match {
      case NavigationWithKearfott => ExtractNavKearfott.from(data)
    }

  }

  implicit class LogRecordsDSL(data: Seq[LogRecord[Double]]) {
    private[this] val missionData = Map(new File("fake.log") -> data)
    private[this] val dsl         = new MissionDataDSL(missionData)

    def as(t: PoseType): Seq[NavigationDatum] = dsl.as(t)

    def as(t: PositionType): Seq[Position] = Nil

    def as(t: BeamType): Seq[Beam] = dsl.as(t)

    def as(t: VelocityType): Seq[KearfottDatum] = dsl.as(t)

    def as(t: ExtendedVelocityType) = dsl.as(t)
  }

  /**
    * Implicit conversion to add monotonic method to Seq of TimeDatum types.
    *
    * @param data The sequence of data (must be implicitly convertable to TimeDatum)
    * @tparam T
    */
  implicit class RemoveDuplicateTimes[T <: TimeDatum: ClassTag](data: Seq[T]) {

    /**
      * Removes log datatypes containing duplicate timestamps
      *
      * @return A Seq in the same order as the original but with duplicate timestamps removed
      */
    def monotonic: Seq[T] = {
      val (_, i, _) = Matlib.unique(data.map(_.time).toArray)
      Matlib
        .subset(data.toArray, ArraySeq.unsafeWrapArray(i))
        .toIndexedSeq
        .sortBy(_.time)(Ordering.Double.IeeeOrdering)
    }

  }

}
