package org.mbari.auv.io.dsl

import org.mbari.auv.io.MissionReader.MissionData
import scilube.Matlib

/**
 * Extracts Navigation and Kearfott data from [[MissionData]]. The Kearfott data is linearly interpolated
 *
 *
 * Created by brian on 6/3/14.
 */
object ExtractNavKearfott extends ExtractAs[NavKearfortDatum] {

  def from(data: MissionData): Seq[NavKearfortDatum] = {
    val nav = (data as Navigation).monotonic
    val k = (data as Kearfott).monotonic
    interpolateToNav(nav, k)
  }

  def interpolateToNav(nav: Seq[NavigationDatum], kearfott: Seq[KearfottDatum]): Seq[NavKearfortDatum] = {
    val navTime = nav.map(_.time).toArray
    val kearfottTime = kearfott.map(_.time).toArray

    // --- Interpolate accelerations
    val ax = kearfott.map(_.ax).toArray
    val ay = kearfott.map(_.ay).toArray
    val az = kearfott.map(_.az).toArray

    val nax = Matlib.interp1(kearfottTime, ax, navTime)
    val nay = Matlib.interp1(kearfottTime, ay, navTime)
    val naz = Matlib.interp1(kearfottTime, az, navTime)

    // Interpolate velocities
    val vx = kearfott.map(_.vx).toArray
    val vy = kearfott.map(_.vy).toArray
    val vz = kearfott.map(_.vz).toArray

    val nvx = Matlib.interp1(kearfottTime, vx, navTime)
    val nvy = Matlib.interp1(kearfottTime, vy, navTime)
    val nvz = Matlib.interp1(kearfottTime, vz, navTime)

    // Generate Nav w/ Velocities
    for (i <- 0 until navTime.size) yield {
      val n = nav(i)
      NavPoseWithVelocity(n.time, n.x, n.y, n.z, n.phi, n.theta, n.psi,
        nvx(i), nvy(i), nvz(i), nax(i), nay(i), naz(i),
        n.gpsValid, n.dvlValid, n.bottomLock, n.crs)
    }
  }

}
