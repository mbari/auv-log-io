package org.mbari.auv.io.dsl

import org.mbari.auv.io.MissionReader.MissionData
import org.slf4j.LoggerFactory
import scala.util.{ Try, Failure, Success }

/**
 *
 * Created by brian on 6/3/14.
 */
object ExtractKearfott extends Extractor with ExtractAs[KearfottDatum] {

  private[this] val log = LoggerFactory.getLogger(getClass)

  def from(data: MissionData): Seq[KearfottDatum] = toPoses(data, Kearfott)

  private def toPoses(data: MissionData, instrumentName: String): Seq[KearfottDatum] = {
    val records = missionToRecords(data, instrumentName)
    if (!records.isEmpty) {
      Try {
        val time = extract(records, "time").get
        val x = extract(records, "mNorthK").get
        val y = extract(records, "mEastK").get
        val z = extract(records, "mDepthK").get

        def munge(v: Option[IndexedSeq[Double]]) = v match {
          case None => time.map(_ => 0D)
          case Some(d) => d
        }

        val phi = munge(extract(records, "mRollK"))
        val theta = munge(extract(records, "mPitchK"))
        val psi = munge(extract(records, "mHeadK"))
        val vx = munge(extract(records, "mVbodyxK"))
        val vy = munge(extract(records, "mVbodyyK"))
        val vz = munge(extract(records, "mVbodyzK"))
        val ax = munge(extract(records, "mAccelxK"))
        val ay = munge(extract(records, "mAccelyK"))
        val az = munge(extract(records, "mAccelzK"))

        for (i <- 0 until time.size) yield {
          KearfottPose(time(i), x(i), y(i), z(i), phi(i), theta(i), psi(i),
            vx(i), vy(i), vz(i), ax(i), ay(i), az(i), Position.UTMZONE10N)
        }

      } match {
        case Success(d) => d
        case Failure(e) =>
          log.warn("Failed to read kearfott data", e)
          Nil
      }
    } else Nil
  }

}
