package org.mbari.auv.io

import java.io.File

import scala.io.Source

/**
 * Very simple parser. Converts lines in a file like:
 *     KEY: value
 *     NAME: rdiDVL
 * to a IndexedSeq of key-value pairs like the following:
 *    IndexedSeq("KEY" -> "value", "NAME" -> "rdiDVL")
 *
 * The order of the keys is preserved so that they match the order in the file. Lines starting with
 * '#' or containing only whitespace are ignored. Invalid lines will cause this parser to throw an
 * Exception. We're assuming that if one line is bogus then the whole file is invalid.
 *
 * @author Brian Schlining
 * @since 2013-08-13
 */
object GenericCfgReader {

  // Match against: KEY: VALUE or KEY:VALUE or KEY: VALUE1 VALUE 2 VALUE3
  private[this] val pattern = "(.+):\\s?(.+)".r

  /**
   * Parse a file into a map of key-value pairs
   * @param file
   * @return
   */
  def parse(file: File): IndexedSeq[(String, String)] = {
    // Drop comment lines and whitespace when reading file
    val lines = Source.fromFile(file).getLines().filter { line =>
      val trimmedLine = line.trim
      !trimmedLine.startsWith("#") && !trimmedLine.isEmpty
    }

    // Map parts of line to key -> value string pairs
    lines.map { line =>
      val pattern(key, value) = line
      key -> value
    }.toIndexedSeq
  }

  /**
   * Convert "30, 30, 30" to IndexedSeq(30D, 30D, 30D)
   */
  def parseArray(s: String): IndexedSeq[Double] = s.split(",\\s?").map(_.toDouble).toIndexedSeq

}
