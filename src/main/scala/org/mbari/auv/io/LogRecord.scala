package org.mbari.auv.io

/**
 *  AUV logs can contain many different data records. This class is a container for a single data record
 *  from a log file.
 *
 *  This container includes both the description, as a [[org.mbari.auv.io.LogRecordDescription]], and the
 *  data, as an [[scala.collection.mutable.IndexedSeq]]
 *
 * @param description The description of the particular record in a AUV log
 * @param data The data for a particular record in an AUV log
 * @tparam A The type of the data for this record
 *
 * @author Brian Schlining
 * @since Sep 7, 2010
 */
class LogRecord[A](val description: LogRecordDescription, val data: IndexedSeq[A]) {

  override def toString(): String = {
    "LogRecord(longName=" + description.longName + ",units=" + description.units +
      ",size=" + data.size + ")"
  }

}
