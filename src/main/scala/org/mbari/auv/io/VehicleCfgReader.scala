package org.mbari.auv.io

import java.io.File

import scala.io.Source

/**
 * @author Brian Schlining
 * @since 2017-02-06T14:44:00
 */
object VehicleCfgReader {

  // Match against: KEY= VALUE or KEY=VALUE or KEY = VALUE or KEY= VALUE1 VALUE 2 VALUE3
  // (any)=(any except ;)(non-capture of everything else)
  private[this] val pattern = "(.+)=\\s?([^;]+)(?:.*)".r

  def parse(file: File): Seq[(String, String)] = {
    // Drop comment lines and whitespace when reading file
    val lines = Source.fromFile(file).getLines().filter { line =>
      val trimmedLine = line.trim
      !trimmedLine.startsWith("//") && !trimmedLine.isEmpty
    }

    // Map parts of line to key -> value string pairs
    lines.map { line =>
      val pattern(key, value) = line
      key.trim -> value.trim
    }.toSeq
  }

}
