package org.mbari.auv.io

import java.io.File
import scala.util.Try

/**
 * Tool for reading a directory of AUV log files
 */
object MissionReader {

  type MissionData = Map[File, Seq[LogRecord[Double]]]
  type MissionHeaders = Map[File, Seq[LogRecordDescription]]

  /**
   * Reads an entire directory of log files from an AUV mission.
   *
   * @param directory The directory of Log files to read
   * @return A Map where the key is the file that was read and the value is all the
   *         [[org.mbari.auv.io.LogRecord]]s read from the file.
   */
  def read(directory: File): MissionData = directory.listFiles()
    .filter(f => f != null && !f.isDirectory && f.getName.endsWith(".log"))
    .flatMap(f => Try(f -> LogRecordReader.read(f)).toOption) // make tuple of file -> List[LogRecord]
    .toMap

  def readHeaders(directory: File): MissionHeaders = directory.listFiles()
    .filter(f => f != null && !f.isDirectory && f.getName.endsWith(".log"))
    .flatMap(f => Try(f -> LogRecordReader.readHeader(f)).toOption) // make tuple of file -> List[LogRecord]
    .toMap

}
