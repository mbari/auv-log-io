package org.mbari.auv.io

/**
 * This is description of a single data type found in an AUV log. Logs contain a header like:
 * {{{
 * # binary gps
 * # timeTag time %8.8e ,time ,UNKNOWN
 * # integer hours %d ,Hours ,Hours
 * # integer minutes %d ,Minutes ,Minutes
 * # integer seconds %d ,Seconds ,Seconds
 * # integer centiSeconds %d ,CentiSeconds ,CentiSeconds
 * # angle latitude %8.8e ,Vehicle latitude ,Degrees
 * # angle longitude %8.8e ,Vehicle longitude ,Degrees
 * # short quality %d ,GPS quality code ,Unitless
 * # short numberOfSatellites %d ,Number of satellites visible ,Unitless
 * }}}
 * In this example, each line other than the 1st is a log record. __gps is the instrumentName
 * for all [[LogRecord]]s in the file. Using __latitude__ as our example it would map to the following
 * fields:
 *   - rawFormat -> "double"
 *   - shortName -> "longitude"
 *   - longName -> "Vehicle longitude"
 *   - units -> Degrees (but I think it's actually storing radians ... error in the log!)
 *   - instrumentName -> "gps"
 * @author Brian Schlining
 * @since 2012-08-28
 */
case class LogRecordDescription(format: String, shortName: String,
    longName: String, units: String, instrumentName: String) {

  /**
   * The format converted to scala
   */
  val scalaFormat = format match {
    case "float" => "Float"
    case "integer" => "Int"
    case "short" => "Short"
    case _ => "Double"
  }

  /**
   * @return the length in bytes of a value for this record
   */
  val length: Int = scalaFormat match {
    case "Float" => 4
    case "Int" => 4
    case "Short" => 2
    case "Double" => 8
  }

}

object LogRecordDescription {

  /**
   * This method does a bit of work to change to replace time units.
   *
   * @param format
   * @param shortName
   * @param longName
   * @param units
   * @param instrumentName
   */
  def create(format: String, shortName: String, longName: String,
    units: String, instrumentName: String): LogRecordDescription = {

    val u = shortName match {
      case "time" => "seconds since 1970-01-01 00:00:00Z"
      case _ => units.replaceAll("[\r\n]", "").trim
    }

    LogRecordDescription(format, shortName, longName, u, instrumentName)

  }
}

