package org.mbari.auv.io

import java.io.{File, FileInputStream}
import java.util.regex.Pattern
import java.nio.charset.Charset
import java.nio.channels.FileChannel
import java.nio.{ByteOrder, MappedByteBuffer}

import collection.mutable
import collection.immutable.IndexedSeq
import org.slf4j.LoggerFactory

import scala.io.Source

/**
  * This object does the actual reading of a file
  *
  * @see http://www.cs.otago.ac.nz/cosc463/random-access.htm
  * @author Brian Schlining
  * @since Sep 7, 2010
  */
object LogRecordReader {

  private[this] val log = LoggerFactory.getLogger(getClass())

  /**
    * Reads a file and returns the records in the log file (including all data)
    *
    * @param file The file to parse
    * @return A List of all records in the log file
    */
  def read(file: File): List[LogRecord[Double]] = {
    //val startTime = System.nanoTime
    log.debug("Reading " + file.getCanonicalPath)
    val fileInputStream = new FileInputStream(file)
    val fileChannel     = fileInputStream.getChannel
    // Get the filessize and map it into memory
    val mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size())
    mappedByteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    // Decode the file into a char buffer
    val (descriptions, position) = readHeader(mappedByteBuffer)
    log.debug("Found " + descriptions.size + " data types")
    //descriptions.foreach { println(_) }
    log.debug("Reading binary data, starting at byte " + position)
    mappedByteBuffer.position(position)

    def readValue(logRecordDescription: LogRecordDescription) = {
      logRecordDescription.scalaFormat match {
        case "Float"  => mappedByteBuffer.getFloat().toDouble
        case "Int"    => mappedByteBuffer.getInt().toDouble
        case "Short"  => mappedByteBuffer.getShort().toDouble
        case "Double" => mappedByteBuffer.getDouble()
      }
    }

    // Read binary data
    val results = if (descriptions.nonEmpty) {
      val records = descriptions.map(_ -> new mutable.ArrayBuffer[Double])
      var i       = 0
      try {
        while (true) {
          records.foreach {
            case (desc, data) =>
              data += readValue(desc)
          }
          i = i + 1
        }
      }
      catch {
        case e: Exception => log.debug("Done. Found " + i + " records")
      }

      //val elapsedTime = (System.nanoTime - startTime) / 1000D / 1000D / 1000D
      //log.debug(String.format("It took %12.9f nano seconds to read %s\n", Array(elapsedTime, file.getCanonicalPath)))

      records.map(t => new LogRecord[Double](t._1, IndexedSeq(t._2.toSeq: _*))) // Return LogRecords
    }
    else {
      Nil
    }

    fileChannel.close()
    fileInputStream.close()
    //ByteBuffers.closeDirectBuffer(mappedByteBuffer)

    results
  }

  /**
    * Parses the ASCII header into something useful
    *
    * @param mappedByteBuffer The buffer of the memory mapped log file
    * @return A tuple with a list of LogRecords that have not yet been populated with data
    *      as well as the byte offset to start reading the data with
    *
    */
  private def readHeader(mappedByteBuffer: MappedByteBuffer): (List[LogRecordDescription], Int) = {

    // --- Returned variables
    var records: List[LogRecordDescription] = Nil
    var numberOfBytes                       = 0
    var instrumentName                      = ""

    // --- Configure decoder for reading header.
    val charsetDecoder = Charset.forName("ISO-8859-15").newDecoder()
    val charBuffer     = charsetDecoder.decode(mappedByteBuffer)
    val linePattern    = Pattern.compile(".*\r?\n")
    val lineMatcher    = linePattern.matcher(charBuffer)

    // --- Read
    var continue = lineMatcher.find()
    while (continue) {
      val line = lineMatcher.group() // The current line
      numberOfBytes = numberOfBytes + line.getBytes.length
      log.debug("--- Parsing: " + line.replace('\r', ' ').replace('\n', ' '))
      val parts = line.split(" ").map(_.trim)
      continue = !parts(1).startsWith("begin")
      if (continue) {
        try {
          if (parts(1).startsWith("binary")) {
            instrumentName = parts(2)
          }
          else {
            val otherParts = line.split(",").map(_.trim)
            records = LogRecordDescription.create(
              parts(1),
              parts(2),
              otherParts(1),
              otherParts(2),
              instrumentName
            ) :: records
          }
        }
        catch {
          case e: Exception => log.debug("\t!!! Invalid line")
        }
        continue = lineMatcher.find()
      }
    }
    (records.reverse, numberOfBytes)
  }

  /**
    * Reads only the header of an AUV log ile
    * @param file The AUV log file to read
    * @return The LogRecordDescriptions describing the contents of the log
    */
  def readHeader(file: File): List[LogRecordDescription] = {
    var records: List[LogRecordDescription] = Nil
    var instrumentName                      = ""
    val source                              = Source.fromFile(file, "ISO-8859-15")
    val lines                               = source.getLines()
    var ok                                  = true
    while (ok) {
      val line  = lines.next()
      val parts = line.split(" ").map(_.trim)
      ok = !parts(1).startsWith("begin")
      if (ok) {
        try {
          if (parts(1).startsWith("binary")) {
            instrumentName = parts(2)
          }
          else {
            val otherParts = line.split(",").map(_.trim)
            records = LogRecordDescription.create(
              parts(1),
              parts(2),
              otherParts(1),
              otherParts(2),
              instrumentName
            ) :: records
          }
        }
        catch {
          case e: Exception => log.debug("\t!!! Invalid line")
        }
      }
    }
    source.close()
    records
  }

}
