package org.mbari.auv.io

import mbarix4j.net.URLUtilities
import org.scalatest.{FunSpec, Matchers}

/**
  * @author Brian Schlining
  * @since 2017-02-06T14:38:00
  */
class GenericCfgReaderSpec extends FunSpec with Matchers {

  describe("GenericCfgReader") {

    it("should parse sensor specifications correctly") {
      val file = URLUtilities.toFile(getClass.getResource("/cfg/auv/imagenex_specs.cfg"))
      val map  = GenericCfgReader.parse(file).toMap
      map should have size 7
      map should (contain key ("NAME") and contain value ("imagenex"))
    }

    it("should parse vehicle specifications correctly") {
      val file = URLUtilities.toFile(getClass.getResource("/cfg/auv/ventana_specs.cfg"))
      val map  = GenericCfgReader.parse(file).toMap
      map should have size 6
      map should (contain key ("NAME") and contain value ("ventana"))
    }
  }
}
