package org.mbari.auv.io

import org.junit.runner.RunWith
import mbarix4j.net.URLUtilities
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FlatSpec, Matchers}

/**
  *
  * @author Brian Schlining
  * @since 2013-08-29
  */
@RunWith(classOf[JUnitRunner])
class LogRecordReaderSpec extends FlatSpec with Matchers {

  var logRecords: List[LogRecord[Double]] = _

  "A LogRecordReader" should "read an AUV gps log" in {
    val url        = getClass.getResource("/data/2010322/2010.322.03/gps.log")
    val file       = URLUtilities.toFile(url)
    val logRecords = LogRecordReader.read(file)
    logRecords.size should be(14)
    for (r <- logRecords) {
      r.data.size should be(142)
    }
    val timeRecord = logRecords.filter(_.description.shortName == "time").head
    timeRecord.data.head should be(1.290103103922357e+09 +- 1)
    logRecords.head.description.instrumentName should be("gps")
  }

  it should "read an AUV navigation log" in {
    val url        = getClass.getResource("/data/2010322/2010.322.03/navigation.log")
    val file       = URLUtilities.toFile(url)
    val logRecords = LogRecordReader.read(file)
    logRecords.size should be(33)
    for (r <- logRecords) {
      r.data.size should be(10256)
    }
  }

  it should "read an AUV navigation log's headers only" in {
    val url        = getClass.getResource("/data/2010322/2010.322.03/navigation.log")
    val file       = URLUtilities.toFile(url)
    val logHeaders = LogRecordReader.readHeader(file)
    logHeaders.size should be(33)
  }

}
