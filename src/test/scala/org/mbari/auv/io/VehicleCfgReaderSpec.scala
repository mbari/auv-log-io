package org.mbari.auv.io

import mbarix4j.net.URLUtilities
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Brian Schlining
  * @since 2017-02-06T14:48:00
  */
class VehicleCfgReaderSpec extends FlatSpec with Matchers {

  "VehicleCfgReader" should "parse" in {
    val file = URLUtilities.toFile(getClass.getResource("/data/2016348/2016.348.00/vehicle.cfg"))
    val map  = VehicleCfgReader.parse(file).toMap
    map should not be empty
    map("vehicleName") should be("Dorado389")
  }

}
