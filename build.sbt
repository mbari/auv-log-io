val junitVersion     = "4.12"
val logbackVersion   = "1.2.3"
val mbarix4jVersion  = "2.0.5.jre11"
val scalatestVersion = "3.0.8"
val scilubeVersion   = "2.0.7.jre11"
val slf4jVersion     = "1.7.26"

Global / onChangedBuildSource := ReloadOnSourceChanges

lazy val buildSettings = Seq(
  organization := "org.mbari.auv",
  version := "1.0.3.jre11",
  scalaVersion := "2.13.1",
  crossScalaVersions := Seq("2.13.1"),
  homepage := Some(url("https://hohonuuli.github.io/auv-log-io/")),
  bintrayOrganization := Some("org-mbari")
)

lazy val consoleSettings = Seq(
  shellPrompt := { state =>
    val user = System.getProperty("user.name")
    user + "@" + Project.extract(state).currentRef.project + ":sbt> "
  },
  initialCommands in console :=
    """
      |import java.time.Instant
      |import java.util.UUID
    """.stripMargin
)

lazy val dependencySettings = Seq(
  resolvers ++= Seq(
    Resolver.mavenLocal,
    Resolver.bintrayRepo("hohonuuli", "maven"),
    Resolver.bintrayRepo("org-mbari", "maven"),
    Resolver.sonatypeRepo("public"),
    "geotoolkit" at "https://maven.geotoolkit.org",
    "unidata" at "https://artifacts.unidata.ucar.edu/repository/unidata-all/"
  )
)

lazy val optionSettings = Seq(
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding",
    "UTF-8", // yes, this is 2 args
    "-feature",
    "-language:existentials",
    "-language:implicitConversions",
    "-unchecked",
    "-Xlint"
  ),
  javacOptions ++= Seq("-target", "11", "-source", "11"),
  updateOptions := updateOptions.value.withCachedResolution(true),
  publishArtifact in Test := false,
  pomIncludeRepository := { _ => false },
  publishMavenStyle in ThisBuild := true,
  licenses in ThisBuild ++= Seq(
    ("MIT", url("http://opensource.org/licenses/MIT"))
  )
)

lazy val appSettings = buildSettings ++
  consoleSettings ++
  dependencySettings ++
  optionSettings ++ Seq(
  fork := true
)

lazy val `auv-log-io` = (project in (file(".")))
  .settings(appSettings)
  .settings(
    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic"     % logbackVersion,
      "ch.qos.logback" % "logback-core"        % logbackVersion,
      "junit"          % "junit"               % junitVersion % "test",
      "org.mbari"      % "mbarix4j"            % mbarix4jVersion % "test",
      "org.scalatest"  %% "scalatest"          % scalatestVersion % "test",
      "org.slf4j"      % "log4j-over-slf4j"    % slf4jVersion,
      "org.slf4j"      % "slf4j-api"           % slf4jVersion,
      "scilube"        %% "scilube-core"       % scilubeVersion,
      "scilube"        %% "scilube-extensions" % scilubeVersion,
      "scilube"        %% "scilube-gis"        % scilubeVersion
    ).map(_.exclude("org.slf4j", "slf4j-jdk14"))
  )

// Aliases
addCommandAlias("cleanall", ";clean;clean-files")
